const express = require('express');
const router = require('./routes');
const path = require('path');
// inisialisasi
const app = express();
// listen for request
const port = 5000;
app.set('view engine', 'ejs');

app.use('/chapter3', express.static(path.join(__dirname, 'chapter3file')))
app.use('/chapter4', express.static(path.join(__dirname, 'challenge-project-4')))

// basic routing
app.get('/', (req, res) => {
    res.send('Hello World!')
  })
  
app.get('/chapter3', (req, res) => {
    res.sendFile('./chapter3file/index.html', { root: __dirname });
  })

app.get('/chapter4', (req, res) => {
    res.sendFile('./challenge-project-4/index.html', { root: __dirname });
  })

app.use('/chapter4', (req, res) => {
    res.sendFile('./challenge-project-4/style.css', { root: __dirname });
  })

// menampilkan users.json
app.get('/users', (req, res) => {
  const users = require('./users_API/users.json')
  function compare(a, b) {
    // Use toUpperCase() to ignore character casing
    const nameA = a.name.toUpperCase();
    const nameB = b.name.toUpperCase();
  
    let comparison = 0;
    if (nameA > nameB) {
      comparison = 1;
    } else if (nameA < nameB) {
      comparison = -1;
    }
    return comparison;
  }
// users.users.sort(compare);
res.json(users.users.sort(compare));
  // res.sendFile('./users.json', { root:__dirname });
})

app.use(router);
  
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
  })
  